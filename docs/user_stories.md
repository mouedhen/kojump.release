# Application Kojump

## Introduction
Kojump est une application qui permet de lister les équipements sportifs en France, ainsi de conseiller à ses utilisateurs les lieux à choisir et ce par les expériences des autres.

## Fonctionnalités
> - Administrateur : personne chargé d'administrer l'aspect applicatif
> - Invité : utilisateur non authentifié de l'application
> - Utilisateur : utilisateur authentifié de l'application
> - Gérant : utilisateur gérant un lieu
> - Développeur : personne chargé de développer l'application
> - Webmaster : personne chargé de déployer l'application
> - Administrateur Systèmes : personne chargé de configurer le système afin de le préparer au déployement de l'application
> - Client : personne possédant l'application
> - Système : tâches automatisées

- [x] **Générales** :
	- [x] En tant que développeur, je veux pouvoir utiliser un environnement de développement portable.
	- [ ] En temps que développeur, je veux pouvoir lancer les tests automatiquement.
	- [x] En tant que webmaster, je veux pouvoir utiliser plusieurs fichiers de configuration afin de prendre en compte les différents environnements (production / développement / OS).
	- [x] En tant qu'utilisateur, je veux pouvoir accéder aux informations relatives à la société en charge de l'application.
	- [x] En tant qu'utilisateur, je veux pouvoir accéder à la politique de confidentialité de mes données.
	- [x] En tant qu'utilisateur, je veux pouvoir savoir l'usage des cookies enregistré sur mon ordinateur.
	- [x] En tant qu'utilisateur, je veux pouvoir envoyer un message à la personne en charge de l'application.
	- [ ] En tant qu'invité, je veux pouvoir accéder à l'application depuis n'importe quel support informatique (ordinateur / tablette / smartphone).
- [ ] **Gestion des utilisateurs** :
	- [x] En tant qu'administrateur, je veux pouvoir gérer la liste des utilisateurs.
	- [x] En tant qu'administrateur, je veux pouvoir activer / désactiver des utilisateurs.
	- [x] En tant qu'invité, je veux pouvoir m'inscrire avec une addresse e-mail afin d'accéder au fonctionnalités fournies par l'application.
	- [x] En tant qu'utilisateur, je veux pouvoir me connecter à mon compte en utilisant mon addresse e-mail et mon mot de passe que j'ai définie lors de l'inscription.
	- [x] En tant qu'utilisateur, je veux pouvoir réinitialiser mon mot de passe en cas d'oublie.
	- [x] En tant qu'utilisateur, je veux pouvoir modifier mes informations personnelles.
	- [ ] En tant qu'utilisateur, je veux pouvoir accéder à mon tableau de bord.
	- [x] En tant qu'utilisateur, je veux pouvoir me déconnecter de mon compte.
	- [ ] En tant qu'utilisateur, je veux que le système de déconnecte en cas de non activité prolongé sur mon compte.
- [ ] **Enregistrement via les réseaux sociaux** :
	- [ ] En tant qu'utilisateur, je veux pouvoir m'enregister via les réseaux sociaux les plus connu afin de faciliter le process.
	- [ ] En tant qu'administrateur, je veux pouvoir configurer les réseaux sociaux liés à l'application.
- [ ] **Publications** :
	- [ ] En tant qu'utilisateur, je veux pouvoir publier mes propre lieux via l'application.
	- [ ] En tant qu'utilisateur, je veux pouvoir créer un "brouillon" de mes lieux avant leur publication afin de me faciliter le saisie des informations
	- [ ] En tant qu'utilisateur, je veux pouvoir visualiser facilement les lieux que j'ai publié et leurs états (brouillon / en attente de validation / validé / actif / innactif).
	- [ ] En tant que gérant, je veux pouvoir savoir la raison d'innactivité d'un lieux dont je suis le proriétaire / gérant.
	- [ ] En tant que gérant, je veux pouvoir réclamer la propriété / la gérance d'un lieux existant.
	- [x] En tant qu'administrateur, je veux pouvoir gérer les publications.
	- [x] En tant qu'administrateur, je veux pouvoir activer / désactiver un lieux.
- [x] **Commentaires** :
	- [x] En tant qu'utilisateur, je veux pouvoir donner mon avis sur les lieux que j'ai visité.
	- [x] En tant qu'invité, je veux pouvoir lire l'avis des autres utilisateurs.
	- [x] En tant qu'invité, je ne peux pas commenter les lieux.
	- [x] En tant qu'administrateur, je veux pouvoir gérer les commentaires.
	- [x] En tant qu'administrateur, je veux pouvoir modérer les commentaires publiés.
	- [x] En tant que développeur, je veux pouvoir accéder à une API REST afin de gérer les comentaires.
	- [x] En tant que développeur, je veux pouvoir intégrer facilement les commentaires sur n'importe qu'elle page.
- [ ] **Evaluation** :
	- [ ] En tant qu'invité, je veux pouvoir visualiser les évaluations des lieux afin de me guider dans mon choix.
	- [ ] En tant qu'utilisateur, je veux pouvoir évaluer les lieux que j'ai visité.
- [ ] **Favoris** :
	- [ ] En tant qu'utlisateur, je veux pouvoir enregistrer les lieux que j'aime afin de les retrouver facilement.
- [x] **Flux RSS** :
	- [x] En tant qu'invité, je veux pouvoir souscrire à un flux RSS afin de visualiser les ajouts dans l'application sur ma propre application.
- [ ] **Suivie d'activité des utilisateurs** :
	- [ ] En tant qu'administrateur, je veux pouvoir suivre l'activité des utilisateurs afin d'améliorer leurs expériences.
	- [ ] En tant qu'administrateur, je veux pouvoir connaître les lieux les plus visité par les utilisateurs.
	- [ ] En tant que système, je veux pouvoir
- [ ] **Recommandation** :
	- [ ] En tant que système, je veux pouvoir déterminer les lieux adaptés aux utilisateurs d'après plusieurs critères. 
	- [ ] En tant qu'invité, je veux pouvoir connaître les meilleurs lieux proposés par l'application.
- [ ] **Gestion des espaces sportifs** :
	- [x] En tant qu'invité, je veux pouvoir visualiser la liste des activités sportives ressencées par l'application.
	- [x] En tant qu'invité, je veux pouvoir visualiser la liste des lieux sportifs en France.
	- [x] En tant qu'invité, je veux pouvoir visualiser les lieux en rapport à une activité choisie.
	- [x] En tant qu'invité, je veux pouvoir visualiser les détails des institutions sportives.
	- [x] En tant qu'invité, je veux pouvoir visualiser les évènements proposés par les institutions sportives.
	- [x] En tant qu'invité, je veux pouvoir connaître les horaires d'ouverture des institutions sportives.
	- [x] En tant qu'invité, je veux pouvoir visualiser des photos des institutions sportives.
	- [ ] En tant que gérant, je veux pouvoir spécifier les horaires d'ouverture de mes établissements.
	- [ ] En tant que gérant, je veux pouvoir promouvoir mes établissements avec des évènements.
	- [x] En tant qu'administrateur, je veux pouvoir gérer les établissements sportifs.
	- [x] En tant qu'administrateur, je veux pouvoir modifier les informations et les détails des établissement sportifs.
	- [x] En tant qu'administrateur, je veux pouvoir activer / désactiver des établissements sportifs.
	- [x] En tant qu'administrateur, je veux pouvoir gérer les activités sprotives.
	- [x] En tant qu'administrateur, je veux pouvoir activer / désactiver des activités sportives.
- [ ] **Géolocalisation** :
	- [x] En tant qu'invité, je veux pouvoir connaître la position géographique des établissements sportifs.
	- [x] En tant qu'invité, je veux pouvoir connaître ma position géographique afin de savoir les établissements sportifs qui sont proches de moi.
	- [x] En tant qu'invité, je veux pouvoir visualiser sur une map les établissements sportifs.
	- [ ] En tant que gérant, je veux pouvoir définir la position géographique de mes établissement sportifs.
	- [x] En tant qu'administrateur, je veux pouvoir gérer les positions géographiques des établissements sportifs.
- [ ] **Recherche** :
	- [ ] En tant qu'invité, je veux pouvoir filtrer les établissements sportifs par disciplines.
	- [ ] En tant qu'invité, je veux pouvoir filtrer les établissements sportifs par privés / publiques.
	- [ ] En tant qu'invité, je veux pouvoir filtrer les établissements sportifs par les prix proposés.
	- [x] En tant qu'invité, je veux pouvoir filtrer les établissements sportifs par leurs positions par rapports à la mienne.
	- [x] En tant qu'invité, je veux pouvoir filtrer les établissements sportifs par leurs positions géographiques.
	- [x] En tant qu'invité, je veux pouvoir filtrer les établissements sportifs par leurs nom.
	- [ ] En tant qu'invité, je veux pouvoir filtrer les établissements sportifs par leurs horaires d'ouverture.
- [x] **Import / Export** :
	- [x] En tant qu'administrateur systèmes, je veux pouvoir importer des fichiers sous le format CSV dans la base de données.
	- [x] En tant qu'administrateur, je pouvoir exporter les données de la base en fichiers CSV.