# Kojump tâches
<style>
    li {color: rgba(41, 128, 185,0.8)}
    li li {color: rgba(44, 62, 80,0.9)}
    li li li {color: rgba(192, 57, 43,0.8)}
	strong {color: rgba(52, 73, 94,0.8)}
</style>
## Tâches Générale

### Générale
- [ ] Initialisation du projet (11)
	- [x] Installation des bibliothèques requises pour le projet
	- [x] Initialisation d'un projet Django
	- [x] Installation des bibliothèques requises pour le développement
	- [x] Installation des bibliothèques requises pour les tests (TDD et BDD)
	- [x] Création d'un fichier requirement.txt
	- [x] Création d'un environnement de développement frontend
	- [x] Création d'un fichier d'automatisation pour webpack
	- [ ] Création d'un fichier d'automatisation pour les tests unitaires
	- [x] Configuration des routes statiques
	- [x] Création d'un mockup d'une page blanche
		- [x] Création de la barre de navigation
		- [x] Création du footer
		- [x] Ajout du support multi plateforme
	- [x] Intégration de la page blanche à l'application
- [x] En tant que webmaster, je veux pouvoir utiliser plusieurs fichiers de configuration afin de prendre en compte les différents environnements (production / développement / OS)
	- [x] Création du module de configuration
	- [x] Création du fichier de configuration de base
	- [x] Création du fichier de configuration de développement
	- [x] Création du fichier de configuration de production
- [x] En tant qu'utilisateur, je veux pouvoir accéder aux informations relatives à la société en charge de l'application
	- [x] Création des tests de comportements (Behaviour Tests)
	- [x] Création des tests unitaires (Unit Tests)
	- [x] Création d'un mockup pour la page **A Propos**
        - [x] Création de la page
        - [x] Ajout du support multi plateforme
	- [x] Integration de la page **A Propos** à l'application
		- [x] Création du gabarit de la page
		- [x] Ajout des urls nécessaires
- [x] En tant qu'utilisateur, je veux pouvoir accéder aux détails législatifs de l'application
	- [x] Création des tests de comportements (Behaviour Tests)
	- [x] Création des tests unitaires (Unit Tests)
	- [x] Création d'un mockup pour la page **Mentions légales**
        - [x] Création de la page
        - [x] Ajout du support multi plateforme
	- [x] Integration de la page **Mentions légales** à l'application
		- [x] Création du gabarit de la page
		- [x] Ajout des urls nécessaires
- [x] En tant qu'utilisateur, je veux pouvoir savoir l'usage des cookies enregistré sur mon ordinateur
	- [x] Création des tests de comportements (Behaviour Tests)
	- [x] Création des tests unitaires (Unit Tests)
	- [x] Création d'un mockup pour la page **Confidentialité & Cookies**
        - [x] Création de la page
        - [x] Ajout du support multi plateforme
	- [x] Integration de la page **Confidentialité & Cookies** à l'application
		- [x] Création du gabarit de la page
		- [x] Ajout des urls nécessaires
- [x] En tant qu'utilisateur, je veux pouvoir envoyer un message à la personne en charge de l'application
	- [x] Création des tests de comportements (Behaviour Tests)
	- [x] Création des tests unitaires (Unit Tests)
	- [x] Création d'un mockup pour la page **Nous contacter**
        - [x] Création de la page
        - [x] Ajout du support multi plateforme
    - [x] Création du formulaire **ContactForm**
    - [x] Création de la vue **ContactFormVue**
	- [x] Integration de la page **Nous contacter** à l'application
		- [x] Création du gabarit de la page
		- [x] Ajout des urls nécessaires


### Gestion des utilisateurs
- [x] En tant qu'administrateur, je veux pouvoir gérer la liste des utilisateurs
- [ ] En tant qu'administrateur, je veux pouvoir activer / désactiver des utilisateurs
	- [ ] Création des tests unitaires (Unit Tests)
	- [x] Etendre la classe afin d'y ajouter le profil utilisateur (class Profile)
	- [x] Modification de l'administration des utilisateurs afin de prendre en considération le profil (classe **Profile**)
	- [x] Création d'un signal pour l'ajout d'un utilisateur pour l'insertion automatique du profil associé dans la base
	- [x] Création d'un signal pour la modification d'un utilisateur
- [ ] En tant qu'invité, je veux pouvoir m'inscrire avec une addresse e-mail afin d'accéder au fonctionnalités fournies par l'application
	- [x] Création des tests unitaires (Unit Tests)
	- [x] Création du formulaire **UserRegistrationForm**
	- [x] Création de la vue **UserRegistrationView**
	- [x] Création d'un mockup de la page **S'inscrire**
	- [x] Intégration de la page **S'inscrire** à l'application
		- [x] Création du gabarit de la page
		- [x] Ajout des urls nécessaires
    - [x] de contraintes pour l'adresse e-mail (est requise / est unique)
    - [ ] Ajout du mécanisme de confirmation de l'adresse e-mail
- [x] En tant qu'utilisateur, je veux pouvoir me connecter à mon compte en utilisant mon addresse e-mail et mon mot de passe que j'ai définie lors de l'inscription
	- [x] Création des tests unitaires (Unit Tests)
    - [x] Création de la vue **UserAuthenticationView**
	- [x] Création d'un mockup de la page **Se connecter**
	- [x] Intégration de la page **Se connecter** à l'application
		- [x] Création du gabarit de la page
		- [x] Ajout des urls nécessaires
- [x] En tant qu'utilisateur, je veux pouvoir réinitialiser mon mot de passe en cas d'oublie
	- [x] Création des tests unitaires (Unit Tests)
    - [x] Création de la vue **UserResetPasswordView**
	- [x] Création d'un mockup de la page **Mot de passe oublié**
	- [x] Création d'un mockup de la page **Réinitialiser le mot de passe**
	- [x] Création d'un mockup pour les messages d'erreurs et de succèss
	- [x] Intégration des mockups pour la réinitialisation du mot de passe à l'application.
    - [x] Ajout des urls nécessaires
- [ ] En tant qu'utilisateur, je veux pouvoir modifier mes informations personnelles
	- [ ] Création des tests de comportements (Behaviour Tests)
	- [ ] Création des tests unitaires (Unit Tests)
    - [x] Création du formulaire **UserForm**
    - [x] Création du formulaire **ProfieView**
    - [x] Création de la vue **UserProfileEditView**
	- [ ] Création d'un mockup de la page **Profil utilisateur**
	- [ ] Intégration de la page **Profil utilisateur** à l'application
		- [ ] Création du gabarit de la page
		- [ ] Ajout des urls nécessaires
- [ ] En tant qu'utilisateur, je veux pouvoir accéder à mon tableau de bord
	- [ ] Création des tests de comportements (Behaviour Tests)
	- [ ] Création des tests unitaires (Unit Tests)
    - [ ] Création de la vue **UserAccountVue**
	- [ ] Création d'un mockup de la page **Compte utilisateur**
	- [ ] Intégration de la page **Compte utilisateur** à l'application
		- [ ] Création du gabarit de la page
		- [ ] Ajout des urls nécessaires
- [x] En tant qu'utilisateur, je veux pouvoir me déconnecter de mon compte
    - [x] Ajout des urls nécessaires
- [x] En tant qu'utilisateur, je veux que le système de déconnecte en cas de non activité prolongé sur mon compte
	- [x] Configuration du temps d'inactivité à 5 minutes

### Enregistrement via les réseaux sociaux @TODO
- [ ] En tant qu'utilisateur, je veux pouvoir m'enregister via les réseaux sociaux les plus connu afin de faciliter le process.
- [ ] En tant qu'administrateur, je veux pouvoir configurer les réseaux sociaux liés à l'application.

### Publications
- [ ] En tant qu'utilisateur, je veux pouvoir publier mes propre lieux via l'application.
- [ ] En tant qu'utilisateur, je veux pouvoir créer un "brouillon" de mes lieux avant leur publication afin de me faciliter le saisie des informations
- [ ] En tant qu'utilisateur, je veux pouvoir visualiser facilement les lieux que j'ai publié et leurs états (brouillon / en attente de validation / validé / actif / innactif).
- [ ] En tant que gérant, je veux pouvoir savoir la raison d'innactivité d'un lieux dont je suis le proriétaire / gérant.
- [ ] En tant que gérant, je veux pouvoir réclamer la propriété / la gérance d'un lieux existant.
- [x] En tant qu'administrateur, je veux pouvoir gérer les publications.
- [x] En tant qu'administrateur, je veux pouvoir activer / désactiver un lieux.

### Commentaires
- [x] En tant qu'utilisateur, je veux pouvoir donner mon avis sur les lieux que j'ai visité.
- [x] En tant qu'invité, je veux pouvoir lire l'avis des autres utilisateurs.
- [x] En tant qu'invité, je ne peux pas commenter les lieux.
- [x] En tant qu'administrateur, je veux pouvoir gérer les commentaires.
- [x] En tant qu'administrateur, je veux pouvoir modérer les commentaires publiés.
- [x] En tant que développeur, je veux pouvoir accéder à une API REST afin de gérer les comentaires.
- [x] En tant que développeur, je veux pouvoir intégrer facilement les commentaires sur n'importe qu'elle page.

### Evaluation
- [ ] En tant qu'invité, je veux pouvoir visualiser les évaluations des lieux afin de me guider dans mon choix.
- [ ] En tant qu'utilisateur, je veux pouvoir évaluer les lieux que j'ai visité.

### Favoris
- [ ] En tant qu'utlisateur, je veux pouvoir enregistrer les lieux que j'aime afin de les retrouver facilement.

### Flux RSS
- [x] En tant qu'invité, je veux pouvoir souscrire à un flux RSS afin de visualiser les ajouts dans l'application sur ma propre application.

### Suivie d'activité des utilisateurs
- [ ] En tant qu'administrateur, je veux pouvoir suivre l'activité des utilisateurs afin d'améliorer leurs expériences.
- [ ] En tant qu'administrateur, je veux pouvoir connaître les lieux les plus visité par les utilisateurs.
- [ ] En tant que système, je veux pouvoir

### Recommandation
- [ ] En tant que système, je veux pouvoir déterminer les lieux adaptés aux utilisateurs d'après plusieurs critères. 
- [ ] En tant qu'invité, je veux pouvoir connaître les meilleurs lieux proposés par l'application.

### Gestion des espaces sportifs
- [x] En tant qu'invité, je veux pouvoir visualiser la liste des activités sportives ressencées par l'application.
- [x] En tant qu'invité, je veux pouvoir visualiser la liste des lieux sportifs en France.
- [x] En tant qu'invité, je veux pouvoir visualiser les lieux en rapport à une activité choisie.
- [x] En tant qu'invité, je veux pouvoir visualiser les détails des institutions sportives.
- [x] En tant qu'invité, je veux pouvoir visualiser les évènements proposés par les institutions sportives.
- [x] En tant qu'invité, je veux pouvoir connaître les horaires d'ouverture des institutions sportives.
- [x] En tant qu'invité, je veux pouvoir visualiser des photos des institutions sportives.
- [ ] En tant que gérant, je veux pouvoir spécifier les horaires d'ouverture de mes établissements.
- [ ] En tant que gérant, je veux pouvoir promouvoir mes établissements avec des évènements.
- [x] En tant qu'administrateur, je veux pouvoir gérer les établissements sportifs.
- [x] En tant qu'administrateur, je veux pouvoir modifier les informations et les détails des établissement sportifs.
- [x] En tant qu'administrateur, je veux pouvoir activer / désactiver des établissements sportifs.
- [x] En tant qu'administrateur, je veux pouvoir gérer les activités sprotives.
- [x] En tant qu'administrateur, je veux pouvoir activer / désactiver des activités sportives.

### Géolocalisation
- [x] En tant qu'invité, je veux pouvoir connaître la position géographique des établissements sportifs.
- [x] En tant qu'invité, je veux pouvoir connaître ma position géographique afin de savoir les établissements sportifs qui sont proches de moi.
- [x] En tant qu'invité, je veux pouvoir visualiser sur une map les établissements sportifs.
- [ ] En tant que gérant, je veux pouvoir définir la position géographique de mes établissement sportifs.
- [x] En tant qu'administrateur, je veux pouvoir gérer les positions géographiques des établissements sportifs.

### Recherche
- [ ] En tant qu'invité, je veux pouvoir filtrer les établissements sportifs par disciplines.
- [ ] En tant qu'invité, je veux pouvoir filtrer les établissements sportifs par privés / publiques.
- [ ] En tant qu'invité, je veux pouvoir filtrer les établissements sportifs par les prix proposés.
- [x] En tant qu'invité, je veux pouvoir filtrer les établissements sportifs par leurs positions par rapports à la mienne.
- [x] En tant qu'invité, je veux pouvoir filtrer les établissements sportifs par leurs positions géographiques.
- [x] En tant qu'invité, je veux pouvoir filtrer les établissements sportifs par leurs nom.
- [ ] En tant qu'invité, je veux pouvoir filtrer les établissements sportifs par leurs horaires d'ouverture.

### Import / Export
- [x] En tant qu'administrateur systèmes, je veux pouvoir importer des fichiers sous le format CSV dans la base de données.
- [x] En tant qu'administrateur, je pouvoir exporter les données de la base en fichiers CSV.