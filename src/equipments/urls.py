# -*- coding: utf-8 -*-

from django.conf.urls import url
from equipments.views.institution import (
    InstitutionListView,
    InstitutionDetailView,
    InstitutionCreateView,
    InstitutionUpdateView,
    InstitutionDeleteView,)

app_name = 'equipments'

urlpatterns = [
    # Intitution views
    url(r'^institutions/$', InstitutionListView.as_view(), name='institution_list'),
    url(r'^institutions/(?P<page>\d+)/$', InstitutionListView.as_view(), name='institution_list'),
    url(r'^institutions/details/(?P<pk>[a-zA-Z0-9]+)/$', InstitutionDetailView.as_view(), name='institution_details'),
    url(r'^institutions/create/$', InstitutionCreateView.as_view(), name='institution_create'),
    url(r'^institutions/update/(?P<pk>[a-zA-Z0-9]+)/$', InstitutionUpdateView.as_view(), name='institution_update'),
    url(r'^institutions/delete/(?P<pk>[a-zA-Z0-9]+)/$', InstitutionDeleteView.as_view(), name='institution_delete'),
]
