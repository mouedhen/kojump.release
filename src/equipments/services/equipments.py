# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.serializers import serialize
import json

from django.views.generic import View
from django.http import HttpResponse

from ..models.equipment import Equipment


class InstitutionEquipmentGeoJson(View):

    def get(self, request, institution_pk):
        data = serialize('geojson', Equipment.objects.filter(institution__code=institution_pk))
        mimetype = 'application/json'
        return HttpResponse(data, mimetype)
