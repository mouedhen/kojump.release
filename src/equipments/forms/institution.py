# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django import forms

from equipments.models.institution import Institution


class InstitutionCreateForm(forms.ModelForm):

    class Meta:
        model = Institution
        exclude = ['code', 'slug', 'is_active']
