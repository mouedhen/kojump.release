# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django import forms

from equipments.models.equipment import Equipment


class EquipmentCreateForm(forms.ModelForm):

    class Meta:
        model = Equipment
        exclude = ['code', 'slug', 'is_active']
