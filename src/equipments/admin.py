# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from equipments.models.activity import Activity
from equipments.models.discipline_spotive import SportDiscipline
from equipments.models.base import (EquipmentCategoryFamily, EquipmentCategory, ActivityLevel, SiteEnvironment,
                                    SiteGround, UsageType, EquipmentTarget, ManagerCategory, SpecialInstitution,
                                    Department, Commune)
from equipments.models.institution import Institution
from equipments.models.equipment import Equipment


admin.site.register(EquipmentCategoryFamily)
admin.site.register(EquipmentCategory)
admin.site.register(ActivityLevel)
admin.site.register(SiteEnvironment)
admin.site.register(SiteGround)
admin.site.register(UsageType)
admin.site.register(EquipmentTarget)
admin.site.register(ManagerCategory)
admin.site.register(SpecialInstitution)
admin.site.register(Department)
admin.site.register(Commune)
admin.site.register(Institution)
admin.site.register(Equipment)


@admin.register(Activity)
class ActivityAdmin(admin.ModelAdmin):

    save_on_top = True
    list_display = ('reference', 'label', 'pin_color', 'is_active',)
    list_filter = ('is_active',)
    search_fields = ('label',)
    prepopulated_fields = {'slug': ('label',)}
    list_editable = ('pin_color',)


@admin.register(SportDiscipline)
class SportDisciplineAdmin(admin.ModelAdmin):
    list_display = ('reference', 'label', 'is_active',)
    list_filter = ('is_active', 'activity',)

    search_fields = ('reference', 'label',)
    prepopulated_fields = {'slug': ('label',)}

    fieldsets = [
        (_('General'), {'fields': ('reference', 'label', 'slug', 'is_active',)}),
        (_('Activity'), {'fields': ('activity',)}),
    ]
