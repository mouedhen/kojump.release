# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-03 06:04
from __future__ import unicode_literals

import colorfield.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipments', '0002_auto_20171003_0558'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activity',
            name='pin_color',
            field=colorfield.fields.ColorField(default='#84e6b3', max_length=18, verbose_name='couleur du pin'),
        ),
    ]
