# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-27 03:20
from __future__ import unicode_literals

import colorfield.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipments', '0009_auto_20171024_0254'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activity',
            name='pin_color',
            field=colorfield.fields.ColorField(default='#17b41f', max_length=18, verbose_name='couleur du pin'),
        ),
    ]
