# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-09 02:15
from __future__ import unicode_literals

import colorfield.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipments', '0007_auto_20171004_0229'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activity',
            name='pin_color',
            field=colorfield.fields.ColorField(default='#34b8f1', max_length=18, verbose_name='couleur du pin'),
        ),
    ]
