# -*- coding: utf-8 -*-
import uuid

from slugify import slugify

from django.db.models.signals import pre_save
from django.dispatch import receiver

from equipments.models.institution import Institution


@receiver(pre_save, sender=Institution)
def create_profile_handler(sender, instance, *args, **kwargs):
    """
    Handle institution pre save
    """
    instance.code = uuid.uuid4().hex[:20]
    instance.slug = slugify(instance.name)
