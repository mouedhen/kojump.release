# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import random
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible

from django.db import models
from colorfield.fields import ColorField


def generate_color():
    color = '#{:02x}{:02x}{:02x}'.format(*map(lambda x: random.randint(0, 255), range(3)))
    return color


@python_2_unicode_compatible
class Activity(models.Model):

    class Meta:
        verbose_name = _(u'Activité')
        verbose_name_plural = _(u'Activités')
        unique_together = ('label', 'slug',)

    reference = models.IntegerField(_(u'reférence'), primary_key=True)
    label = models.CharField(_(u'label'), max_length=254)
    slug = models.SlugField(_(u'slug'), max_length=254)
    description = models.TextField(_(u'description'), blank=True, null=True)
    image = models.ImageField(_(u'image'), blank=True, null=True, upload_to='activity')
    pin_color = ColorField(_(u'couleur du pin'), default=generate_color())
    is_active = models.BooleanField(_('est active ?'), default=False)

    def __str__(self):
        return self.label
