# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible

from django.db import models
from equipments.models.activity import Activity


@python_2_unicode_compatible
class SportDiscipline(models.Model):

    reference = models.PositiveIntegerField(_('reférence unique'), primary_key=True, default=0)
    label = models.CharField(_(u'label'), max_length=254)
    slug = models.SlugField(_(u'slug'), max_length=254)
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE, related_name='sports_disciplines')
    is_active = models.BooleanField(_('est active ?'), default=False)

    class Meta:
        verbose_name = _('discipline sportive')
        verbose_name_plural = _('disciplines sportives')
        unique_together = ('label', 'slug',)

    @property
    def pin_color(self):
        if self.activity:
            return self.activity.pin_color
        return None

    def __str__(self):
        return self.label
