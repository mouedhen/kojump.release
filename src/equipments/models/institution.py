# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible

from django.db import models

from core.models import SluggedModel, DescribedModel, TimeStampedModel, ActivatedModel
from equipments.models.base import SpecialInstitution, Commune
from schedule.models import TimeInterval
from geolocations.models import GeoLocation


@python_2_unicode_compatible
class Institution(models.Model):
    # General
    code = models.CharField(_('code'), max_length=20, primary_key=True)
    name = models.CharField(_('nom'), max_length=254)
    slug = models.SlugField(_('slug'), max_length=254)
    thumbnail = models.ImageField(_('thumbnail'), blank=True, null=True,
                                  default='default-institution-thumbnail.png',
                                  upload_to='institutions/thumbnail')
    special_institution = models.ForeignKey(SpecialInstitution, on_delete=models.CASCADE, related_name='institutions',
                                            blank=True)
    # Address
    street_number = models.CharField(_('num. rue'), max_length=10, blank=True, null=True)
    street_name = models.CharField(_('nom rue'), max_length=100, blank=True)
    common_name = models.CharField(_('nom commun'), max_length=80, blank=True)
    postal_code = models.CharField(_('code postal'), max_length=10, blank=True)
    commune = models.ForeignKey(Commune, on_delete=models.CASCADE, related_name='institution', blank=True)
    # Additional information
    have_internet = models.BooleanField(_('a internet ?'), blank=True, default=False)
    number_covers = models.IntegerField(_('nombre de couvers'), blank=True, default=0, null=True)
    number_beds = models.IntegerField(_('nombre de lits'), blank=True, default=0, null=True)
    number_parking_spaces = models.IntegerField(_('nombre de places de parking'), blank=True, default=0, null=True)
    # Disabled person accessibility information
    is_accessible_for_hand_m = models.BooleanField(_('accéssible aux handicapés à mobilité réduite ?'),
                                                   default=False)
    is_accessible_for_hand_s = models.BooleanField(_('accéssible aux handicapés sensoriaux ?'), default=False)
    number_parking_hn_spaces = models.IntegerField(_('nombre de places de parking pour les personnes handicapés'),
                                                   blank=True, default=0, null=True)
    # Transport information
    have_metro = models.BooleanField(_('accéssible en métro ?'), blank=True, default=False)
    have_bus = models.BooleanField(_('accéssible en autobus ?'), blank=True, default=False)
    have_tramway = models.BooleanField(_('accéssible en tramway ?'), blank=True, default=False)
    have_train = models.BooleanField(_('accéssible en train ?'), blank=True, default=False)
    have_boat = models.BooleanField(_('accéssible en bateau ?'), blank=True, default=False)
    have_other_transport = models.BooleanField(_('a transport (autre) ?'), blank=True, default=False)
    # Administration
    is_active = models.BooleanField(_('est actif ?'), default=False)
    # Important dates
    created = models.DateTimeField(_('date de création'), auto_now_add=True)
    modified = models.DateTimeField(_('date de modification'), auto_now=True)

    class Meta:
        verbose_name = _('institution')
        verbose_name_plural = _('institutions')

    def __str__(self):
        return self.name

    @property
    def coordinates(self):
        equipment = self.equipments.first()
        if equipment:
            return equipment.gps_coordinates
        return None

    @property
    def pin_color(self):
        equipment = self.equipments.first()
        if equipment:
            return equipment.pin_color
        return None

    @property
    def address(self):
        return '{} {} - {} {}'.format(self.street_number, self.street_name, self.postal_code, self.commune.department)


class InstitutionImage(models.Model):
    institution = models.ForeignKey(Institution, related_name='images')
    image = models.ImageField(_('image'), 'default-institution.png',
                              upload_to='institutions/images')
