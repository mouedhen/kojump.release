# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible

from django.db import models

from core.models import SluggedModel, TimeStampedModel


@python_2_unicode_compatible
class EquipmentCategoryFamily(SluggedModel, TimeStampedModel):

    class Meta:
        verbose_name = _('famille d\'équipements')
        verbose_name_plural = _('famille d\'équipements')
        unique_together = ('label', 'slug',)

    def __str__(self):
        return self.label


@python_2_unicode_compatible
class EquipmentCategory(SluggedModel, TimeStampedModel):

    family = models.ForeignKey(EquipmentCategoryFamily, on_delete=models.CASCADE, related_name='categories')

    class Meta:
        verbose_name = _('catégorie d\'équipements')
        verbose_name_plural = _('catégories d\'équipements')
        unique_together = ('label', 'slug',)

    def __str__(self):
        return self.label


@python_2_unicode_compatible
class ActivityLevel(SluggedModel):

    class Meta:
        verbose_name = _('level de l\'activité')
        verbose_name_plural = _('levels des activités')
        unique_together = ('label', 'slug',)

    def __str__(self):
        return self.label


@python_2_unicode_compatible
class SiteEnvironment(SluggedModel):

    class Meta:
        verbose_name = _('environnement du site')
        verbose_name_plural = _('environnements des sites')
        unique_together = ('label', 'slug',)

    def __str__(self):
        return self.label


@python_2_unicode_compatible
class SiteGround(SluggedModel):

    class Meta:
        verbose_name = _('type du sol')
        verbose_name_plural = _('types des sols')
        unique_together = ('label', 'slug',)

    def __str__(self):
        return self.label


@python_2_unicode_compatible
class UsageType(SluggedModel):

    class Meta:
        verbose_name = _('type d\'utilisation')
        verbose_name_plural = _('types d\'utilisation')
        unique_together = ('label', 'slug',)

    def __str__(self):
        return self.label


@python_2_unicode_compatible
class EquipmentTarget(SluggedModel):

    class Meta:
        verbose_name = _('publique cible')
        verbose_name_plural = _('publique cibles')
        unique_together = ('label', 'slug',)

    def __str__(self):
        return self.label


@python_2_unicode_compatible
class ManagerCategory(SluggedModel):

    IS_PUBLIC_CHOICES = (
        ('NOT_DEFINED', _('non définie')),
        ('IS_PUBLIC', _('oui')),
        ('NOT_PUBLIC', _('non')),
    )

    is_public = models.CharField(_('est publique ?'), max_length=15, choices=IS_PUBLIC_CHOICES, default='NOT_DEFINED')

    class Meta:
        verbose_name = _('categorie du gestionnaire')
        verbose_name_plural = _('categories des gestionnaires')
        unique_together = ('label', 'slug',)

    def __str__(self):
        return self.label


@python_2_unicode_compatible
class SpecialInstitution(SluggedModel):

    class Meta:
        verbose_name = _('institution spéciale')
        verbose_name_plural = _('institutions spéciales')
        unique_together = ('label', 'slug',)

    def __str__(self):
        return self.label


@python_2_unicode_compatible
class Department(SluggedModel):
    code = models.CharField(_('code'), max_length=15, primary_key=True)

    class Meta:
        verbose_name = _('département')
        verbose_name_plural = _('départements')

    def __str__(self):
        return self.label


@python_2_unicode_compatible
class Commune(SluggedModel):
    code_insee = models.CharField(_('code INSEE'), max_length=15, primary_key=True)
    department = models.ForeignKey(Department, on_delete=models.CASCADE, related_name='communes', blank=True)

    class Meta:
        verbose_name = _('commune')
        verbose_name_plural = _('communes')

    def __str__(self):
        return self.label
