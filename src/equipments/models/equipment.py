# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible

from django.db import models
from equipments.models.base import SiteGround, SiteEnvironment, ManagerCategory, EquipmentCategory
from equipments.models.institution import Institution
from equipments.models.discipline_spotive import SportDiscipline
from schedule.models import TimeInterval
from geolocations.models import GeoLocation


@python_2_unicode_compatible
class Equipment(GeoLocation):
    # General
    code = models.CharField(_('code'), max_length=12, primary_key=True)
    name = models.CharField(_('nom'), max_length=254)
    slug = models.CharField(_('slug'), max_length=254)
    thumbnail = models.ImageField(_('thumbnail'), blank=True, null=True,
                                  default='default-equipment-thumbnail.png',
                                  upload_to='equipments/thumbnail')
    description = models.TextField(_('description'), blank=True, null=True)
    institution = models.ForeignKey(Institution, on_delete=models.CASCADE,
                                    related_name='equipments', blank=True, null=True)
    # Management
    owner_category = models.ForeignKey(ManagerCategory, on_delete=models.SET_NULL,
                                       related_name='owned_equipment', blank=True, null=True)
    manager_category = models.ForeignKey(ManagerCategory, on_delete=models.SET_NULL,
                                         related_name='managed_equipment', blank=True, null=True)
    # Category
    category = models.ForeignKey(EquipmentCategory, on_delete=models.SET_NULL,
                                 related_name='equipments', blank=True, null=True)
    # Additional category information
    is_erp_cts = models.BooleanField(_('Chapitau tente ?'), default=False, blank=True)
    is_erp_ref = models.BooleanField(_('Etablissement flottant ?'), default=False, blank=True)
    is_erp_l = models.BooleanField(_('Salle polyvalente ?'), default=False, blank=True)
    is_erp_n = models.BooleanField(_('Restaurant et débit de boisson ?'), default=False, blank=True)
    is_erp_o = models.BooleanField(_('Hôtel ?'), default=False, blank=True)
    is_erp_oa = models.BooleanField(_('Hôtel restaurant d\'altitude ?'), default=False, blank=True)
    is_erp_p = models.BooleanField(_('Salle de danse et jeux ?'), default=False, blank=True)
    is_erp_pa = models.BooleanField(_('Etablissement en plein air ?'), default=False, blank=True)
    is_erp_r = models.BooleanField(_('Enseignement et colo ?'), default=False, blank=True)
    is_erp_rpe = models.BooleanField(_('PE ?'), default=False, blank=True)
    is_erp_sg = models.BooleanField(_('Structure gonflable ?'), default=False, blank=True)
    is_erp_x = models.BooleanField(_('Etablissement sportif couvert ?'), default=False, blank=True)
    # Specification
    have_showers = models.BooleanField(_('a douche ?'), default=False, blank=True)
    have_lights = models.BooleanField(_('a lumière ?'), default=False, blank=True)
    tribune_places = models.IntegerField(_('nombre de places dans la tribune'), default=0, blank=True)
    ground = models.ForeignKey(SiteGround, on_delete=models.SET_NULL,
                               related_name='equipments', blank=True, null=True)
    environment = models.ForeignKey(SiteEnvironment, on_delete=models.SET_NULL,
                                    blank=True, null=True)
    have_heated_cloakroom = models.BooleanField(_('a vestiaires chauffées ?'), default=False, blank=True)
    corridors_number = models.IntegerField(_('nombre de coulloir'), default=0, blank=True, null=True)
    # Utils
    is_always_open = models.BooleanField(_('7d/7d - 24h/24h ?'), default=False, blank=True)
    only_season = models.BooleanField(_('ouvert selement dans les saisons ?'), default=False, blank=True)
    # Target
    for_schools_use = models.BooleanField(_('pour utilisation scolaire ?'), default=False, blank=True)
    for_clubs_use = models.BooleanField(_('pour utilisation des clubs ?'), default=False, blank=True)
    for_others_use = models.BooleanField(_('pour utilisation autre ?'), default=False, blank=True)
    for_individual_use = models.BooleanField(_('pour utilisation individuelle ?'), default=False, blank=True)
    # Disabled person accessibility information
    accessible_handicapped_m = models.BooleanField(_('accéssible aux handicapés à mobilité réduite ?'),
                                                   default=False, blank=True)
    accessible_handicapped_s = models.BooleanField(_('accéssible aux handicapés sensoriaux ?'),
                                                   default=False, blank=True)
    evolution_area_access_hm = models.BooleanField(
        _('Zone d\'évolution accéssible aux handicapés à mobilité réduite ?'),
        default=False, blank=True)
    evolution_area_access_hs = models.BooleanField(
        _('Zone d\'évolution accéssible aux handicapés sensoriaux ?'),
        default=False, blank=True)
    # Additional information
    is_public = models.BooleanField(_('est publique ?'), default=False, blank=True)
    work_timetable = models.ManyToManyField(TimeInterval, blank=True, related_name='equipments')
    price = models.TextField(_('liste des prix'), blank=True, null=True)
    overview = models.TextField(_('vue d\'ensemble'), blank=True, null=True)
    phone_number = models.CharField(_('numéro de téléphone'), max_length=20, blank=True, null=True)
    mail = models.EmailField(_('adresse e-mail'), max_length=50, blank=True, null=True)
    website_url = models.URLField(_('site web'), max_length=80, blank=True, null=True)
    facebook_url = models.URLField(_('facebook'), max_length=80, blank=True, null=True)
    twitter_url = models.URLField(_('twitter'), max_length=80, blank=True, null=True)
    google_plus_url = models.URLField(_('google plus'), max_length=80, blank=True, null=True)
    # activities
    disciplines = models.ManyToManyField(SportDiscipline, blank=True, related_name='equipments')
    # Promotions & events
    promo_event = models.TextField(_('promos & évênements'), blank=True, null=True)
    # Administration
    is_active = models.BooleanField(_('est actif ?'), default=False)
    # Important dates
    created = models.DateTimeField(_('date de création'), auto_now_add=True)
    modified = models.DateTimeField(_('date de modification'), auto_now=True)

    class Meta:
        verbose_name = _('site sportif')
        verbose_name_plural = _('sites sportifs')

    @property
    def pin_color(self):
        discpline = self.disciplines.first()
        if discpline:
            return discpline.pin_color
        return None

    def __str__(self):
        return self.name


class EquipmentImage(models.Model):
    equipment = models.ForeignKey(Equipment, related_name='equipments')
    image = models.ImageField(_('image'), 'default-equipment.png',
                              upload_to='equipments/images')