# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json

from django.views.generic import ListView, DetailView, View
from django.http import HttpResponse

from equipments.forms.activity import MultiSelectActivitiesForm
from equipments.models.activity import Activity
from equipments.models.institution import Institution


class ActiveActivityListView(ListView):

    model = Activity
    template_name = 'site/home.html'

    paginate_by = 6

    def get_queryset(self):
        return Activity.objects.filter(is_active=True)

    def get_context_data(self, **kwargs):
        context = super(ActiveActivityListView, self).get_context_data(**kwargs)
        context['form_activities'] = MultiSelectActivitiesForm
        return context


class ActivityDetailView(DetailView):

    model = Activity
    template_name = 'site/activity-detail.html'


class ActivityInstitutionsAPI(View):

    def get(self, request, activity_pk=1):

        activity = Activity.objects.filter(is_active=True).get(pk=activity_pk)
        institutions = Institution.objects.filter(equipments__disciplines__activity=activity).distinct()

        results = []
        palette = ['#556270', '#4ECDC4', '#C7F464', '#FF6B6B', '#C44D58']
        icon = ['star', 'marker']
        import random

        for institution in institutions:
            if institution.coordinates:
                r5 = random.randint(0, 4)
                r2 = random.randint(0, 1)
                institution_json = {
                    'type': 'Feature',
                    'geometry': {
                        'type': 'Point',
                        'coordinates': [institution.coordinates.x, institution.coordinates.y]
                    },
                    'properties': {
                        'id': institution.pk,
                        'thumbnail': institution.thumbnail.url,
                        'name': institution.name,
                        'slug': institution.slug,
                        'address': institution.address,
                        "marker-color": palette[r5],
                        # "marker-size": "large",
                        "marker-symbol": icon[r2]
                    }
                }
                results.append(institution_json)
        geojson_data = {
            'type': 'FeatureCollection',
            'crs': {
                'type': 'name',
                'properties': {'name': 'EPSG:4326'}
            },
            'features': results
        }
        data = json.dumps(geojson_data)
        mimetype = 'application/json'
        return HttpResponse(data, mimetype)

