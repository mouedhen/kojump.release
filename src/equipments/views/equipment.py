# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, CreateView, ListView, UpdateView, DeleteView

from equipments.forms.equipment import EquipmentCreateForm
from equipments.models.equipment import Equipment


class EquipmentListView(LoginRequiredMixin, ListView):

    model = Equipment
    template_name = 'equipments/institution-list.html'
    paginate_by = 20

    def get_queryset(self):
        return Equipment.objects\
            .values('code', 'name', 'institution__name', 'city', 'postal_code', 'is_active', 'is_public')\
            .filter(institution__post__publisher=self.request.user).order_by('name')


class EquipmentDetailView(LoginRequiredMixin, DetailView):

    model = Equipment
    template_name = 'equipments/institution-details.html'


class EquipmentCreateView(LoginRequiredMixin, CreateView):

    model = Equipment
    template_name = 'equipments/institution-create.html'
    form_class = EquipmentCreateForm


class EquipmentUpdateView(LoginRequiredMixin, UpdateView):

    model = Equipment
    template_name = 'equipments/institution-update.html'

    def get_success_url(self):
        return reverse_lazy('institution_details', self.request.pk)


class EquipmentDeleteView(LoginRequiredMixin, DeleteView):

    model = Equipment
    success_url = reverse_lazy('institution_list')
