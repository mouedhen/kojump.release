# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, CreateView, ListView, UpdateView, DeleteView

from equipments.forms.institution import InstitutionCreateForm
from equipments.models.institution import Institution
from publishing.models.post import Post
from publishing.models.comment import Comment
from publishing.forms.comment import CreateCommentForm


class InstitutionPublicDetailView(DetailView):

    model = Institution
    template_name = 'equipments/institution-public.html'

    def get_context_data(self, **kwargs):
        context = super(InstitutionPublicDetailView, self).get_context_data(**kwargs)
        post, created = Post.objects.get_or_create(
            title=context['institution'].name, slug=context['institution'].slug, institution=context['institution'])
        context['post'] = post
        context['form_comment'] = CreateCommentForm
        return context

    @method_decorator(login_required)
    def post(self, request, pk):
        post = Post.objects.get(institution=self.get_object())
        comment = Comment(publisher=request.user, post=post)
        comment_form = CreateCommentForm(request.POST, instance=comment)
        if comment_form.is_valid():
            comment_form.save()
        return redirect(self.get_success_url(pk))

    @staticmethod
    def get_success_url(pk):
        return reverse_lazy('institution_details_public', kwargs={'pk': pk})


class InstitutionListView(LoginRequiredMixin, ListView):

    model = Institution
    template_name = 'equipments/institution-list.html'
    paginate_by = 20

    def get_queryset(self):
        return Institution.objects\
            .values('code', 'name', 'postal_code', 'commune__label', 'created', 'post__status',
                    'post__verif_status', 'post__is_active')\
            .filter(post__publisher=self.request.user).order_by('name')


class InstitutionDetailView(LoginRequiredMixin, DetailView):

    model = Institution
    template_name = 'equipments/institution-details.html'


class InstitutionCreateView(LoginRequiredMixin, CreateView):

    model = Institution
    template_name = 'equipments/institution-create.html'
    form_class = InstitutionCreateForm


class InstitutionUpdateView(LoginRequiredMixin, UpdateView):

    model = Institution
    template_name = 'equipments/institution-update.html'

    def get_success_url(self):
        return reverse_lazy('institution_details', self.request.pk)


class InstitutionDeleteView(LoginRequiredMixin, DeleteView):

    model = Institution
    success_url = reverse_lazy('institution_list')
