# -*- coding: utf-8 -*-

from django.conf.urls import url, include
from django.contrib import admin

from django.conf.urls.static import static
from django.conf import settings
from django.views.decorators.cache import cache_page

from django.views.generic import TemplateView
from core import views as base_views
from equipments.views.activity import ActiveActivityListView, ActivityDetailView, ActivityInstitutionsAPI
from equipments.views.institution import InstitutionPublicDetailView
from searchengine.views import SearchView
from searchengine.services import SearchService
from equipments.services.equipments import InstitutionEquipmentGeoJson


urlpatterns = [

    # base urls
    url(r'^$', ActiveActivityListView.as_view(), name='home'),
    url(r'^(?P<page>\d+)$', ActiveActivityListView.as_view(), name='home'),

    url(r'^activities/(?P<pk>[0-9]+)$', cache_page(60 * 15)(ActivityDetailView.as_view()), name='activity-detail'),
    url(r'^api/activities/(?P<activity_pk>\d+)/institutions/$', cache_page(60 * 15)(ActivityInstitutionsAPI.as_view()),
        name='api.activity.institutions'),

    url(r'^institutions/(?P<pk>[a-zA-Z0-9]+)$', InstitutionPublicDetailView.as_view(), name='institution_details_public'),
    url(r'^api/institutions/(?P<institution_pk>[a-zA-Z0-9]+)/equipments$', InstitutionEquipmentGeoJson.as_view(), name='api.institutions.equipments'),

    url(r'^search/$', cache_page(60 * 15)(SearchView.as_view()), name='search'),
    url(r'^api/search/$', cache_page(60 * 15)(SearchService.as_view()), name='api.search'),

    url(r'^about/$', cache_page(60 * 15)(TemplateView.as_view(template_name='site/about.html')), name='about'),
    url(r'^contact-us/$', cache_page(60 * 15)(base_views.ContactFormView.as_view()), name='contact'),
    url(r'^legal/copyright-policy/',
        cache_page(60 * 15)(TemplateView.as_view(template_name='legal/copyright-policy.html')),
        name='copyright'),
    url(r'^legal/privacy-policy/', cache_page(60 * 15)(TemplateView.as_view(template_name='legal/privacy-policy.html')), name='privacy'),

    # temporary urls
    url(r'^white/$', cache_page(60 * 15)(TemplateView.as_view(template_name='base.html')), name='blank'),
    # admin urls
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('accounts.urls', namespace='accounts')),
    url(r'^publications/', include('publishing.urls', namespace='publications')),
    url(r'^accounts/equipments/', include('equipments.urls', namespace='equipments')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# if settings.DEBUG:
#     import debug_toolbar
#     urlpatterns = [
#         url(r'^__debug__/', include(debug_toolbar.urls)),
#     ] + urlpatterns
