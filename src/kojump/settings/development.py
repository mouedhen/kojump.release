# -*- coding: utf-8 -*-
from .base import *

DEBUG = True

ALLOWED_HOSTS = []

INSTALLED_APPS += [
    # Django debug toolbar
    # 'debug_toolbar',
]

INTERNAL_IPS = [u'127.0.0.1']

SPATIALITE_LIBRARY_PATH = 'mod_spatialite'

# MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware', ]

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DEFAULT_FROM_EMAIL = 'support@kojump.dev'
DEFAULT_RECIPIENT_LIST = ['contact@kojump.dev']
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = False
EMAIL_PORT = 1025

# Logger used for profiling the application in development
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'color'
        },
    },
    'formatters': {
        'color': {
            '()': 'colorlog.ColoredFormatter',
            'format': "%(log_color)s%(levelname)-8s%(red)s%(module)-30s%(reset)s %(blue)s%(message)s",
        }
    },
    'loggers': {
        '': {
            'level': 'INFO',
            'handlers': ['console'],
        },
        'gunicorn.access': {
            'handlers': ['console']
        },
        'gunicorn.error': {
            'handlers': ['console']
        },
        'django.db.backends': {
            'level': 'INFO',
            'handlers': ['console'],
        }
    },
}
