# -*- coding: utf-8 -*-

from django.core import mail
from django.core.urlresolvers import reverse
from django.test import TestCase


class AboutViewTests(TestCase):
    """
    Test the AboutView
    """

    def setUp(self):
        self.response = self.client.get(reverse('about'))

    def test_view_url_exist_at_desired_location(self):
        """
        Test if the view url exist at the desired location
        :return:
        """
        response = self.client.get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        """
        Test if the view url is accessible by name
        :return:
        """
        self.assertEqual(self.response.status_code, 200)

    def test_view_uses_correct_template(self):
        """
        Test if the view uses the correct template
        :return:
        """
        self.assertEqual(self.response.status_code, 200)
        self.assertTemplateUsed(self.response, 'about.html')


class CopyrightPolicyViewTests(TestCase):
    """
    Test the CopyrightPolicyView
    """

    def test_view_url_exist_at_desired_location(self):
        """
        Test if the view url exist at the desired location
        :return:
        """
        response = self.client.get('/legal/copyright-policy/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        """
        Test if the view url is accessible by name
        :return:
        """
        response = self.client.get(reverse('copyright'))
        self.assertEqual(response.status_code, 200)

    def test_view_use_correct_template(self):
        """
        Test if the view uses the correct template
        :return:
        """
        response = self.client.get(reverse('copyright'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'legal/copyright-policy.html')


class PrivacyPolicyViewTest(TestCase):
    """
    Test the PrivacyPolicyView
    """

    def test_view_url_exist_at_desired_location(self):
        """
        Test if the view url exist at the desired location
        :return:
        """
        response = self.client.get('/legal/privacy-policy/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        """
        Test if the view url is accessible by name
        :return:
        """
        response = self.client.get(reverse('privacy'))
        self.assertEqual(response.status_code, 200)

    def test_view_use_correct_template(self):
        """
        Test if the view uses the correct template
        :return:
        """
        response = self.client.get(reverse('privacy'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'legal/privacy-policy.html')


class ContactViewTests(TestCase):
    """
    Test the ContactView
    """

    def test_view_url_exist_at_desired_location(self):
        """
        Test if the view url exist at the desired location
        :return:
        """
        response = self.client.get('/contact-us/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        """
        Test if the view url is accessible by name
        :return:
        """
        response = self.client.get(reverse('contact'))
        self.assertEqual(response.status_code, 200)

    def test_view_use_correct_template(self):
        """
        Test if the view uses the correct template
        :return:
        """
        response = self.client.get(reverse('contact'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'contact-us.html')

    def test_send_mail(self):
        """
        Valid data through the view results in a success send.
        :return:
        """
        contact_url = reverse('contact')
        data = {
            'name': 'Test',
            'email': 'test@kojump.dev',
            'subject': 'Test case',
            'message': 'Unit test for the ContactForm',
        }
        response = self.client.post(contact_url, data=data)
        self.assertRedirects(response, reverse('contact'))

        self.assertEqual(1, len(mail.outbox))

        message = mail.outbox[0]
        self.assertEqual(data['email'], message.from_email)
        self.assertEqual(data['subject'], message.subject)
        self.assertTrue(data['name'] in message.body)
        self.assertTrue(data['message'] in message.body)

    def test_invalid_data(self):
        """
        Invalid data doesn't work.
        :return:
        """
        contact_url = reverse('contact')
        data = {
            'name': 'Test',
            'subject': 'Test case',
            'message': 'Unit test for the ContactForm',
        }
        response = self.client.post(contact_url, data=data)
        self.assertEqual(200, response.status_code)
        self.assertFormError(response, 'form', 'email', u'Ce champ est obligatoire.')
        self.assertEqual(0, len(mail.outbox))
