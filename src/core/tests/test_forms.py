# -*- coding: utf-8 -*-

from django.test import TestCase

from core.forms import ContactForm


class ContactFormTests(TestCase):
    """
    Test the ContactForm
    """

    def test_valid_data_required(self):
        form = ContactForm(data={
            'name': 'Test',
            'email': 'test@kojump.dev',
            'subject': 'Test case',
            'message': 'Unit test for the ContactForm',
        })
        self.assertTrue(form.is_valid())

    def test_name_required(self):
        form = ContactForm(data={
            'email': 'test@kojump.dev',
            'subject': 'Test case',
            'message': 'Unit test for the ContactForm',
        })
        self.assertFalse(form.is_valid())

    def test_email_required(self):
        form = ContactForm(data={
            'name': 'test',
            'subject': 'Test case',
            'message': 'Unit test for the ContactForm',
        })
        self.assertFalse(form.is_valid())

    def test_email_must_have_correct_form(self):
        form = ContactForm(data={
            'name': 'Test',
            'email': 'test',
            'subject': 'Test case',
            'message': 'Unit test for the ContactForm',
        })
        self.assertFalse(form.is_valid())

    def test_subject_required(self):
        form = ContactForm(data={
            'name': 'Test',
            'email': 'test@kojump.dev',
            'message': 'Unit test for the ContactForm',
        })
        self.assertFalse(form.is_valid())

    def test_message_required(self):
        form = ContactForm(data={
            'name': 'Test',
            'email': 'test@kojump.dev',
            'subject': 'Test case',
        })
        self.assertFalse(form.is_valid())
