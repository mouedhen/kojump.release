# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django import template

register = template.Library()

STATUS = {
    'DRAFT': _(u'Brouillon'),
    'PUB': _(u'Publié(e)')
}


@register.filter(name='statusToString')
def status_to_string(value):
    if STATUS.has_key(value):
        return STATUS[value]
    else:
        return _(u'Non défini')
