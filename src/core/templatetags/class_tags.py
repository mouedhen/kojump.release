# -*- coding: utf-8 -*-
from django import template

register = template.Library()


@register.filter(name='addClass')
def add_class(field, cls):
    return field.as_widget(attrs={"class": cls})
