(function () {
    var institution_id = window.location.href.substr(window.location.href.lastIndexOf('/') + 1);

    L.mapbox.accessToken = 'pk.eyJ1IjoibW91ZWRoZW4iLCJhIjoiY2o1b25ibHdvMDFrbTJxcXRtaWttZ3VqcCJ9.D5l-yqWthlUroyn5GFoY2w';

    var map = L.mapbox.map('map', 'mapbox.streets')
        .setView([48.856638, 2.352241], 6);

    var featureLayer = L.mapbox.featureLayer()
        .loadURL('/api/institutions/' + institution_id + '/equipments')
        .addTo(map);

    featureLayer.on('ready', function (e) {
        this.eachLayer(function (marker) {
            marker.bindPopup('<div><h1><a href="/equipments/' + marker.feature.properties.id + '">' +
                marker.feature.properties.name + '</a></h1>\n' +
                '</div>\n');
            console.log(marker)
        });
    });
})();