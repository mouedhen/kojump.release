var activity_id = window.location.href.substr(window.location.href.lastIndexOf('/') + 1);

L.mapbox.accessToken = 'pk.eyJ1IjoibW91ZWRoZW4iLCJhIjoiY2o1b25ibHdvMDFrbTJxcXRtaWttZ3VqcCJ9.D5l-yqWthlUroyn5GFoY2w';

var map = L.mapbox.map('map', 'mapbox.streets')
    .setView([48.856638, 2.352241], 6);

var featureLayer = L.mapbox.featureLayer()
    .loadURL('/api/activities/6/institutions')
    .on('ready', function (layer) {
        this.eachLayer(function (marker) {
            marker.bindPopup('<div><a href="/institutions/' + marker.feature.properties.id + '">' +
                marker.feature.properties.name + '</a>\n' + '<p>' + marker.feature.properties.address + '</p>\n' +
                '</div>\n');
        });
    })
    .addTo(map);


var featureLayer2 = L.mapbox.featureLayer().loadURL('/api/activities/1/institutions')
    .on('ready', function (layer) {
        this.eachLayer(function (marker) {
            marker.bindPopup('<div><a href="/institutions/' + marker.feature.properties.id + '">' +
                marker.feature.properties.name + '</a>\n' + '<p>' + marker.feature.properties.address + '</p>\n' +
                '</div>\n');
        });
    })
    .addTo(map);

//var groupLayer = L.mapbox.control([featureLayer, featureLayer2]).addTo(map)

var renderFeature = function (feature) {
    var item = document.createElement('div');
    item.classList.add('activity');
    item.innerHTML = '<div class="uk-card uk-card-default uk-card-hover">\n' +
        '                        <div class="uk-card-media-top">\n' +
        '                            <img src="' + feature.properties.thumbnail + '" alt="' + feature.properties.slug + '">\n' +
        '                        </div>\n' +
        '                        <div class="uk-card-body">\n' +
        '                            <h3 class="uk-card-title"><a href="/institutions/' + feature.properties.id + '" style="color:' +
        feature.properties['marker-color'] + ' !important">' +
        feature.properties.name + '</a></h3>\n' +
        '                            <p>' + feature.properties.address + '</p>\n' +
        '                        </div>\n' +
        '                    </div>';
    return item;
};

function onmove() {
    var bounds = map.getBounds();

    document.getElementById('map-elements').innerHTML = '';
    featureLayer.eachLayer(function (marker) {
        if (bounds.contains(marker.getLatLng())) {
            document.getElementById('map-elements').appendChild(renderFeature(marker.feature))
        }
    });
    featureLayer2.eachLayer(function (marker) {
        if (bounds.contains(marker.getLatLng())) {
            document.getElementById('map-elements').appendChild(renderFeature(marker.feature))
        }
    });
}

map.on('moveend', onmove);
featureLayer.on('ready', function (e) {
    L.control.locate().addTo(map);
    map.addControl(L.mapbox.geocoderControl('mapbox.places', {
        autocomplete: true
    }));
    onmove();
});

featureLayer2.on('ready', function (e) {
    L.control.locate().addTo(map);
    map.addControl(L.mapbox.geocoderControl('mapbox.places', {
        autocomplete: true
    }));
    onmove();
});
