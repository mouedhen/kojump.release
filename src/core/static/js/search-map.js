(function () {

    function getAllUrlParams(url) {
        var queryString = url ? url.split('?')[1] : window.location.search.slice(1);
        var obj = {};
        if (queryString) {
            queryString = queryString.split('#')[0];
            var arr = queryString.split('&');
            for (var i = 0; i < arr.length; i++) {
                var a = arr[i].split('=');
                var paramNum = undefined;
                var paramName = a[0].replace(/\[\d*\]/, function (v) {
                    paramNum = v.slice(1, -1);
                    return '';
                });
                var paramValue = typeof(a[1]) === 'undefined' ? true : a[1];
                paramName = paramName.toLowerCase();
                paramValue = decodeURIComponent(paramValue);
                if (obj[paramName]) {
                    if (typeof obj[paramName] === 'string') {
                        obj[paramName] = [obj[paramName]];
                    }
                    if (typeof paramNum === 'undefined') {
                        obj[paramName].push(paramValue);
                    }
                    else {
                        obj[paramName][paramNum] = paramValue;
                    }
                }
                else {
                    obj[paramName] = paramValue;
                }
            }
        }
        return obj;
    }

    function defineActivityAsChecked(activity_id) {
        document.getElementById(activity_id).checked = true;
    }

    function generateActivitiesUrlParams(activities) {
        if (activities instanceof Array) {
            var result = '';
            activities.forEach(function (activity) {
                result += '&activities=' + activity;
            });
            return result
        }
        if (activities !== undefined) {
            return '&activities=' + activities;
        }
        return '';
    }

    var renderFeature = function (feature) {
        var item = document.createElement('div');
        item.classList.add('activity');
        item.innerHTML = '<div class="uk-card uk-card-default uk-card-hover">\n' +
            '                        <div class="uk-card-media-top">\n' +
            '                            <img src="' + feature.properties.thumbnail + '" alt="' + feature.properties.slug + '">\n' +
            '                        </div>\n' +
            '                        <div class="uk-card-body">\n' +
            '                            <h3 class="uk-card-title"><a href="/institutions/' + feature.properties.id + '" style="color:' + feature.properties['marker-color'] + ' !important">' + feature.properties.name + '</a></h3>\n' +
            '                            <p>' + feature.properties.address + '</p>\n' +
            '                        </div>\n' +
            '                    </div>';
        return item;
    };

    function prepareUrl(lat, lng, distance, activities, is_public) {
        var baseUrl = '/api/search/?lat=' + lat + '&lng=' + lng + '&distance=' + (distance / 1000) +
            generateActivitiesUrlParams(activities)

        if (is_public !== undefined && is_public === true) {
            baseUrl += '&is_public=on'
        }
        return baseUrl;
    }

    if (typeof Array.isArray === 'undefined') {
        Array.isArray = function (obj) {
            return Object.prototype.toString.call(obj) === '[object Array]';
        }
    }

    var activities = getAllUrlParams().activities,
        lat = getAllUrlParams().lat,
        lng = getAllUrlParams().lng,
        place = getAllUrlParams().place,
        distance = getAllUrlParams().distance,
        is_public = getAllUrlParams().is_public,
        slider = $('.range-slider'),
        range = $('.range-slider__range');

    if (place === undefined) {
        place = 'Paris, France'
    }
    if (lat === undefined) {
        lat = 44.90191
    }
    if (lng === undefined) {
        lng = -0.47306
    }
    if (distance === undefined) {
        distance = 5000
    } else {
        distance = parseInt(distance) * 1000
    }

    is_public = is_public !== undefined && is_public === 'on';

    if (activities instanceof Array) {
        activities.forEach(function (activity) {
            defineActivityAsChecked('id_activities_' + activity)
        })
    } else {
        if (activities !== undefined) {
            defineActivityAsChecked('id_activities_' + activities)
        }
    }

    document.getElementById('address-search').value = place;
    document.getElementById('lng-search').value = lng;
    document.getElementById('lat-search').value = lat;
    document.getElementById('distance-search').value = distance / 1000;
    document.getElementById('is_public').checked = is_public;

    L.mapbox.accessToken = 'pk.eyJ1IjoibW91ZWRoZW4iLCJhIjoiY2o1b25ibHdvMDFrbTJxcXRtaWttZ3VqcCJ9.D5l-yqWthlUroyn5GFoY2w';

    var map = L.mapbox
        .map('searchMap', 'mapbox.streets', {scrollWheelZoom: false})
        .setView([lat, lng], 11);

    L.control.locate({keepCurrentZoomLevel: true, flyTo: true}).addTo(map);

    var directions = L.mapbox.directions();
    L.mapbox.directions.layer(directions, {readonly: true}).addTo(map);
    L.mapbox.directions.inputControl('inputs', directions).addTo(map);
    L.mapbox.directions.errorsControl('errors', directions).addTo(map);
    L.mapbox.directions.routesControl('routes', directions).addTo(map);
    L.mapbox.directions.instructionsControl('instructions', directions).addTo(map);

    document.getElementById('instructions-control').addEventListener('click', function () {
        var x = document.getElementById("directions");
        if (x.style.display === "none" || x.style.display === '') {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    });

    var featureLayer = L.mapbox.featureLayer()
        .loadURL(prepareUrl(lat, lng, distance, activities, is_public))
        .addTo(map);

    var circle = L.circle(new L.LatLng(lat, lng), distance).addTo(map);

    var marker = L.marker(new L.LatLng(lat, lng), {
        icon: L.mapbox.marker.icon({
            'marker-color': '#3bb2d0',
            'marker-size': 'large',
            'marker-symbol': 'embassy'
        }),
        draggable: true})
        .addTo(map);

    marker.on('dragstart', function () {
        document.getElementById('progress-bar').style.display = 'block';
        circle.setRadius(1);
    });

    marker.on('dragend', function (e) {
        circle.setLatLng(e.target.getLatLng()).setRadius(distance);
        lng = e.target.getLatLng().lng;
        lat = e.target.getLatLng().lat;
        document.getElementById('lng-search').value = lng;
        document.getElementById('lat-search').value = lat;
        featureLayer.loadURL(prepareUrl(lat, lng, distance, activities, is_public,));
        directions.setOrigin(e.target.getLatLng());
        if (directions.queryable()) {
            directions.query();
        }
    });

    featureLayer.on('ready', function (e) {
        document.getElementById('search-list').innerHTML = '';
        directions.setOrigin(new L.LatLng(lat, lng));
        this.eachLayer(function (marker) {
            marker.bindPopup('<div><h1 class="uk-h1"><a href="/institutions/' + marker.feature.properties.id + '">' +
                marker.feature.properties.name + '</a></h1>\n' + '<p>' + marker.feature.properties.address + '</p>\n' +
                '</div>\n');
            marker.on('click', function () {
                directions.setDestination(this.getLatLng());
                directions.query()
            });
            document.getElementById('search-list').appendChild(renderFeature(marker.feature))
        });

        document.getElementById('progress-bar').style.display = 'none';
    });

    map.on('locationfound', function (e) {
        marker.setLatLng(e.latlng);
        circle.setLatLng(e.latlng).setRadius(distance);
        lng = e.latlng.lng;
        lat = e.latlng.lat;
        document.getElementById('lng-search').value = lng;
        document.getElementById('lat-search').value = lat;
        featureLayer.loadURL(prepareUrl(lat, lng, distance, activities, is_public));
        directions.setOrigin(e.latlng);
        if (directions.queryable()) {
            directions.query();
        }
    });

    slider.each(function () {

        $('.range-slider__value').each(function () {
            $(this).html(distance / 1000);
        });

        range.on('input', function () {
            document.getElementById('progress-bar').style.display = 'block';
            distance = this.value * 1000;
            circle.setRadius(distance);
            featureLayer.loadURL(prepareUrl(lat, lng, distance, activities, is_public))
        });
    });

})();