//import L from 'mapbox.js'
if (document.getElementById('map') !== null) {
    let activity_id = window.location.href.substr(window.location.href.lastIndexOf('/') + 1);
    import('mapbox.js').then((L) => {

        L.mapbox.accessToken = 'pk.eyJ1IjoibW91ZWRoZW4iLCJhIjoiY2o1b25ibHdvMDFrbTJxcXRtaWttZ3VqcCJ9.D5l-yqWthlUroyn5GFoY2w';
        let map = L.mapbox.map('map', 'mapbox.streets').setView([40, -74.50], 9);

        let featureLayer = L.mapbox.featureLayer()
            .loadURL('/api/activities/' + activity_id + '/institutions')
            .addTo(map);

        let renderFeature = function (feature) {
            let item = document.createElement('div');
            item.classList.add('activity');
            item.innerHTML = '<div class="uk-card uk-card-default uk-card-hover">\n' +
                '                        <div class="uk-card-media-top">\n' +
                '                            <img src="' + feature.properties.thumbnail + '" alt="' + feature.properties.slug + '">\n' +
                '                        </div>\n' +
                '                        <div class="uk-card-body">\n' +
                '                            <h3 class="uk-card-title"><a href="/institutions/' + feature.properties.id + '" style="color:' +
                feature.properties['marker-color'] + ' !important">' +
                feature.properties.name + '</a></h3>\n' +
                '                            <p>' + feature.properties.address + '</p>\n' +
                '                        </div>\n' +
                '                    </div>';
            return item;
        };

        function onmove() {
            let bounds = map.getBounds();

            document.getElementById('institutions').innerHTML = '';
            featureLayer.eachLayer(function (marker) {
                if (bounds.contains(marker.getLatLng())) {
                    document.getElementById('map-elements').appendChild(renderFeature(marker.feature))
                }
            });
        }

        map.on('moveend', onmove);
        featureLayer.on('ready', function (e) {
            L.control.locate().addTo(map);
            map.addControl(L.mapbox.geocoderControl('mapbox.places', {
                autocomplete: true
            }));
            onmove();
        });
    })
}