require('./hero-slider');
window.addEventListener('load', function () {

    require('./sticky-map');

    import('places.js').then((places) => {

        function generateUrl(url, params) {
            let i = 0, key;
            for (key in params) {
                if (i === 0) {
                    url += "?";
                } else {
                    url += "&";
                }
                url += key;
                url += '=';
                url += params[key];
                i++;
            }
            return url;
        }

        let placesAutoComplete = places({
            container: document.querySelector('#address-input'),
            language: 'fr',
            countries: ['fr'],
        });

        // @TODO send data to the server
        placesAutoComplete.on('change', function (e) {
            let url = generateUrl(location.origin + '/search/', {
                postalcode: e.suggestion.hit.postcode[0],
                lat: e.suggestion.hit._geoloc.lat,
                lng: e.suggestion.hit._geoloc.lng,
                place: e.suggestion.value,
            });

            window.location.replace(url)
        });

        let element = document.getElementById('address-search');
        if (typeof(element) !== 'undefined' && element !== null) {
            let placesSearchAutoComplete = places({
                container: element,
                language: 'fr',
                countries: ['fr'],
            });
            placesSearchAutoComplete.on('change', function (e) {
                console.log(e.suggestion);
                document.getElementById('lng-search').value = e.suggestion.hit._geoloc.lng;
                document.getElementById('lat-search').value = e.suggestion.hit._geoloc.lat;
            });
        }

    });
    require('./hero-text-rotate');
    require('./sport-autocomplete');
    require('./map-control');
    require('./img-slider');
    require('./equipments-rating');

    if (document.getElementById('map-control') !== null) {
        import('./map-control').then(({default: MapControl}) => {
            let control = new MapControl(
                document.getElementById('map-control'),
                document.getElementById('map-elements-container'),
                document.getElementById('map-elements'),
                document.getElementById('map-container'),
                document.getElementById('toggle-elements'),
                document.getElementById('toggle-map')
            );
            control.render('map-elements', 'bottom-scroll-spy');
        })
    }
});