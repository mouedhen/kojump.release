import RatingStars from './RatingStars'

new RatingStars(
    document.querySelector('.global-rate'),
    document.querySelector('.global-star'),
    document.getElementsByClassName('global-star')
);
new RatingStars(
    document.querySelector('.hygiene-rate'),
    document.querySelector('.hygiene-star'),
    document.getElementsByClassName('hygiene-star')
);
new RatingStars(
    document.querySelector('.services-rate'),
    document.querySelector('.services-star'),
    document.getElementsByClassName('services-star')
);
new RatingStars(
    document.querySelector('.equipments-rate'),
    document.querySelector('.equipments-star'),
    document.getElementsByClassName('equipments-star')
);