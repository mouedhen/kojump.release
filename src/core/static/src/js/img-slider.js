import $ from 'jquery'

if (document.getElementById('imgSlider')) {
    import('slick-carousel').then(() => {

        $('.slider').slick({
            arrows: false,
            dots: false,
            infinite: true,
            slidesToShow: 1,
            centerMode: true,
            variableWidth: true,
            autoplay: true,
            autoplaySpeed: 3500,
        })
    });
}