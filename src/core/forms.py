# -*- coding: utf-8 -*-
from django import forms
from django.core.mail import EmailMessage
from django.template.loader import get_template


class ContactForm(forms.Form):
    name = forms.CharField(label=u'Nom & Prénom', required=True)
    email = forms.EmailField(label=u'Adresse e-mail', required=True)
    subject = forms.CharField(label=u'Le sujet de votre demande', required=True)
    message = forms.CharField(label=u'Votre demande', required=True, widget=forms.Textarea)

    def send_mail(self, recipient_list):

        if not self.is_valid():
            raise ValueError(
                "Cannot generate Context from invalid contact form"
            )

        data = self.cleaned_data
        template = get_template('mails/contact_template.txt')
        context = {
            'name': data['name'],
            'email': data['email'],
            'subject': data['subject'],
            'message': data['message'],
        }
        content = template.render(context)

        email = EmailMessage(
            subject=data['subject'],
            from_email=data['email'],
            to=recipient_list,
            body=content,
            headers={'Reply-To': data['email']}
        )
        email.send(fail_silently=False)
