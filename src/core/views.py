# -*- coding: utf-8 -*-
from django.shortcuts import reverse
from django.views.generic.edit import FormView
from django.conf import settings
from core.forms import ContactForm


class ContactFormView(FormView):
    form_class = ContactForm
    recipient_list = settings.DEFAULT_RECIPIENT_LIST
    template_name = 'site/contact-us.html'

    def form_valid(self, form):
        form.send_mail(self.recipient_list)
        return super(ContactFormView, self).form_valid(form)

    def get_success_url(self):
        return reverse('contact')
