# -*- coding: utf-8 -*-
from django.db.models.signals import post_save
from django.dispatch import receiver
# from django.core.exceptions import ValidationError

from django.contrib.auth.models import User
from accounts.models import Profile


@receiver(post_save, sender=User)
def create_profile_handler(sender, instance, created, **kwargs):
    """
    Handle profile creation on user creation
    """
    if not created:
        return None
    profile = Profile(user=instance)
    profile.save()


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


# @receiver(post_save, sender=User)
# def unique_email_handler(sender, instance, created, **kwargs):
#     """
#     Handle email validation (required / unique)
#     """
#     email = instance.email
#     username = instance.username
#
#     if not email:
#         raise ValidationError('email address required')
#     if sender.objects.filter(email=email).exclude(username=username).count() > 0:
#         raise ValidationError('email address must be unique')
