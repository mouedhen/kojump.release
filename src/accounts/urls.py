# -*- coding: utf-8 -*-

from django.conf.urls import url
from django.contrib.auth import views as auth_views
from accounts.views import auth as accounts_views
from accounts.views import profile as profile_views

app_name = 'accounts'

urlpatterns = [
    # Accounts connection urls
    url(r'^register/$', accounts_views.UserRegistrationView.as_view(), name='register'),
    url(r'^login/$', accounts_views.UserAuthenticationView.as_view(), name='login'),
    url(r'^logout/$', auth_views.LogoutView.as_view(), name='logout'),

    # Accounts reset password urls
    url(r'^reset/$', accounts_views.UserResetPasswordView.as_view(), name='password_reset'),
    url(r'^reset/done/$',
        auth_views.PasswordResetDoneView.as_view(template_name='accounts/password_reset_done.html'),
        name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        accounts_views.UserPasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    url(r'^reset/complete/$',
        auth_views.PasswordResetCompleteView.as_view(template_name='accounts/password_reset_complete.html'),
        name='password_reset_complete'),

    # Accounts profile urls
    url(r'^profile/$', profile_views.UserProfileView.as_view(), name='profile'),
    url(r'^profile/edit', profile_views.UserProfileEditView.as_view(), name='profile_edit')
]
