# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.db import models

from django.contrib.auth.models import User

GENDER_CHOICES = (
    ('M', _('homme')),
    ('F', _('femme')),
    ('N', _('non spécifié')),
)


@python_2_unicode_compatible
class Profile(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number = models.CharField(_(u'téléphone'), max_length=16, blank=True, null=True)
    mobile_number = models.CharField(_(u'mobile'), max_length=16, blank=True, null=True)
    fax_number = models.CharField(_(u'fax'), max_length=16, blank=True, null=True)
    street_address = models.CharField(_(u'adresse postale'), max_length=254, blank=True)
    street_address2 = models.CharField(_(u'adresse postale (complément)'), max_length=254, blank=True)
    postal_code = models.CharField(_(u'code postal'), max_length=10, blank=True)
    city = models.CharField(_(u'ville'), max_length=100, blank=True)
    country = models.CharField(_(u'pays'), max_length=100, blank=True)
    photo = models.ImageField(_(u'image de profil'), upload_to='users/%Y/%m/%d', blank=True)
    birth_date = models.DateField(_(u'date de naissance'), blank=True, null=True)
    gender = models.CharField(_('sexe'), max_length=1, choices=GENDER_CHOICES, default='N')
    bio = models.TextField(_(u'présentation'), max_length=1024, null=True, blank=True)
    email_confirmed = models.BooleanField(_(u'adresse e-mail confirmé ?'), default=False)

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = 'profil utilisateur'
        verbose_name_plural = 'profils utilisateurs'
