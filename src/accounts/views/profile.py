# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _

from django.shortcuts import reverse, get_object_or_404, redirect, render
from django.contrib import messages

from django.views.generic import TemplateView, DetailView
from accounts.forms.profile import UserForm, ProfileForm
from django.contrib.auth.mixins import LoginRequiredMixin

from django.contrib.auth.models import User


class UserProfileView(LoginRequiredMixin, DetailView):

    model = User
    template_name = 'profile/profile.html'

    def get_object(self, queryset=None):
        return get_object_or_404(User, pk=self.request.user.pk)

    def get_context_data(self, **kwargs):
        if 'object' not in kwargs:
            kwargs['user_form'] = UserForm(instance=self.get_object())

        return super(UserProfileView, self).get_context_data(**kwargs)


class UserProfileEditView(LoginRequiredMixin, TemplateView):

    template_name = 'profile/profile_edit.html'
    profile_form = ProfileForm
    user_form = UserForm

    def get_user(self):
        user = get_object_or_404(User, pk=self.request.user.pk)
        return user

    def get_context_data(self, **kwargs):
        if 'user_form' not in kwargs:
            kwargs['user_form'] = UserForm(instance=self.get_user())

        if 'profile_form' not in kwargs:
            kwargs['profile_form'] = ProfileForm(instance=self.get_user().profile)
        return super(UserProfileEditView, self).get_context_data(**kwargs)

    def post(self, request):
        user_form = UserForm(request.POST, instance=request.user)
        profile_form = ProfileForm(request.POST, request.FILES, instance=request.user.profile)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, _(u'Votre profil a été mis-à-jour avec succès !'))
            return redirect(self.get_success_url())
        messages.error(request, _(u'Merci de vérifier les informations ci-dessous !'))
        return render(request, self.template_name, {'user_form': user_form, 'profile_form': profile_form})

    @staticmethod
    def get_success_url():
        return reverse('accounts:profile')
