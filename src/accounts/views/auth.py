# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _

from django.shortcuts import reverse
from django.contrib import messages
from django.views.generic import CreateView
from django.contrib.auth.views import LoginView, PasswordResetView, PasswordResetConfirmView

from django.contrib.auth.forms import AuthenticationForm, PasswordResetForm
from accounts.mixins import AnonymousRequiredMixin
from accounts.forms.auth import UserRegistrationForm


class UserRegistrationView(AnonymousRequiredMixin, CreateView):
    form_class = UserRegistrationForm
    template_name = 'accounts/register.html'

    def get_success_url(self):
        messages.success(self.request, _(u'Votre profil a été créé avec success !'))
        return reverse('home')


class UserAuthenticationView(AnonymousRequiredMixin, LoginView):

    form_class = AuthenticationForm
    template_name = 'accounts/authentication.html'


class UserResetPasswordView(AnonymousRequiredMixin, PasswordResetView):

    form_class = PasswordResetForm
    template_name = 'accounts/password_reset.html'
    email_template_name = 'mails/password_reset_email.html'
    subject_template_name = 'mails/password_reset_subject.txt'

    def get_success_url(self):
        return reverse('accounts:password_reset_done')


class UserPasswordResetConfirmView(AnonymousRequiredMixin, PasswordResetConfirmView):

    template_name = 'accounts/password_reset_confirm.html'

    def get_success_url(self):
        return reverse('accounts:password_reset_complete')