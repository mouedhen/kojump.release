# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django import forms
from django.db import transaction

from django.contrib.auth.forms import UserCreationForm

from django.contrib.auth.models import User


class UserRegistrationForm(UserCreationForm):
    first_name = forms.CharField(label=_(u'Prénom'), max_length=30, required=False, )
    last_name = forms.CharField(label=_(u'Nom'), max_length=30, required=False, )
    email = forms.EmailField(label=_(u'Adresse e-mail'), max_length=254, required=False,
                             help_text=u'Merci de fournir une adresse e-mail valide')

    street_address = forms.CharField(label=_(u'Adresse postale'), max_length=254, required=False)
    street_address2 = forms.CharField(label=_(u'Adresse postale (complément)'), max_length=254, required=False)
    city = forms.CharField(label=_(u'Ville'), max_length=100, required=False)
    country = forms.CharField(label=_(u'Pays'), max_length=100, required=False)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2',
                  'street_address', 'street_address2', 'city', 'country')

    def save(self, commit=True):
        with transaction.atomic():
            user = super(UserRegistrationForm, self).save()
            user.refresh_from_db()
            user.profile.street_address = self.cleaned_data.get('street_address')
            user.profile.street_address2 = self.cleaned_data.get('street_address2')
            user.profile.city = self.cleaned_data.get('city')
            user.profile.contry = self.cleaned_data.get('country')
            user.profile.save()
            return user

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if email and User.objects.filter(email=email).count() > 0:
            raise forms.ValidationError(_(u'L\'adresse email saisie existe déjà.'))
        return email
