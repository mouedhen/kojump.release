# -*- coding: utf-8 -*-
from django.test import TestCase

from accounts.forms import UserRegistrationForm


class UserRegistrationFormTests(TestCase):

    def test_form_has_fields(self):
        form = UserRegistrationForm()
        expected = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2',
                    'street_address', 'street_address2', 'city', 'country']
        actual = list(form.fields)
        self.assertSequenceEqual(expected, actual)
