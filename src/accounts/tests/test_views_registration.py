# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse, resolve
from django.core import mail
from django.test import TestCase

from django.contrib.auth.models import User
from django.contrib.auth import views as auth_views

from accounts.forms import UserRegistrationForm
from django.contrib.auth.forms import PasswordResetForm


class UserRegistrationViewTests(TestCase):

    def setUp(self):
        self.response = self.client.get(reverse('accounts:register'))

    def test_view_url_exist_at_desired_location(self):
        """
        Test if the view url exist at the desired location
        :return:
        """
        response = self.client.get("/accounts/register/")
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        """
        Test if the view url is accessible by name
        :return:
        """
        self.assertEqual(self.response.status_code, 200)

    def test_view_uses_correct_template(self):
        """
        Test if the view uses the correct template
        :return:
        """
        self.assertEqual(self.response.status_code, 200)
        self.assertTemplateUsed(self.response, 'accounts/register.html')

    def test_view_contains_csrf(self):
        self.assertContains(self.response, 'csrfmiddlewaretoken')

    def test_view_contain_form(self):
        form = self.response.context.get('form')
        self.assertIsInstance(form, UserRegistrationForm)

    def test_form_inputs(self):
        self.assertContains(self.response, 'type="text"', 7)
        self.assertContains(self.response, 'type="email"', 1)
        self.assertContains(self.response, 'type="password"', 2)


class ValidUserRegistrationTests(TestCase):

    def setUp(self):
        url = reverse('accounts:register')
        data = {
            'username': 'test',
            'password1': 'PLDJZhjh45',
            'password2': 'PLDJZhjh45',
            'email': 'test@kojump.dev'
        }
        self.response = self.client.post(url, data)
        self.home_url = reverse('home')

    def test_registration_status_code(self):
        self.assertEqual(self.response.status_code, 302)

    def test_redirection(self):
        self.assertRedirects(self.response, self.home_url)

    def test_user_creation(self):
        self.assertTrue(User.objects.exists())

    #  TODO authenticate user on registration
    # def test_user_authentication(self):
    #     response = self.client.get(self.home_url)
    #     user = response.context.get('user')
    #     self.assertTrue(user.is_authenticated)


class InvalidUserRegistrationTests(TestCase):

    def setUp(self):
        url = reverse('accounts:register')
        self.response = self.client.post(url, {})

    def test_registration_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_form_errors(self):
        form = self.response.context.get('form')
        self.assertTrue(form.errors)

    def test_dont_create_user(self):
        self.assertFalse(User.objects.exists())
