from django.core import mail
from django.contrib.auth.models import User
from django.urls import reverse
from django.test import TestCase


class PasswordResetMailTests(TestCase):

    def setUp(self):
        User.objects.create_user(username='test', email='test@kojump.dev', password='12345agjk')
        self.response = self.client.post(reverse('accounts:password_reset'), {'email': 'test@kojump.dev'})
        self.email = mail.outbox[0]

    def test_email_subject(self):
        self.assertEqual('[Kojump] Reset password', self.email.subject)

    def test_email_body(self):
        context = self.response.context
        token = context.get('token')
        uid = context.get('uid')
        password_reset_token_url = reverse('accounts:password_reset_confirm', kwargs={
            'uidb64': uid,
            'token': token
        })
        self.assertIn(password_reset_token_url, self.email.body)
        self.assertIn('test', self.email.body)
        self.assertIn('test@kojump.dev', self.email.body)

    def test_email_to(self):
        self.assertEqual(['test@kojump.dev', ], self.email.to)
