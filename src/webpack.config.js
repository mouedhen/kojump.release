const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const ManifestPlugin = require('webpack-manifest-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const dev = process.env.NODE_ENV === 'dev';

let cssLoader = [
    {
        loader: 'css-loader',
        options: {
            importLoaders: 1,
            minimize: true
        }
    },
    {
        loader: 'postcss-loader',
        options: {
            plugins: (loader) => [
                require('autoprefixer')({
                    browsers: ['last 2 versions', 'ie > 8']
                }),
            ]
        }
    },
];

let config = {
    entry: {
        app: ["./core/static/src/scss/app.scss", "./core/static/src/js/app.js"]
    },
    output: {
        path: path.resolve("./core/static/assets"),
        filename: '[name].bundle.js',
        publicPath: "/static/assets/"
    },
    resolve: {
        alias: {
            '@css': path.resolve('./core/static/src/css/'),
            '@': path.resolve('./core/static/src/js/'),
        }
    },
    watch: dev,
    devtool: dev ? "cheap-module-eval-source-map" : "source-map",
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: ['babel-loader']
            },
            {
                test: /\.css$/,
                exclude: /(node_modules|bower_components)/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: cssLoader
                })
            },
            {
                test: /\.scss$/,
                exclude: /(node_modules|bower_components)/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [...cssLoader, 'sass-loader']
                })
            },
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: '[name].bundle.css',
        }),
        new CleanWebpackPlugin(['/core/static/assets'], {
            root: path.resolve('./'),
            verbose: true,
            // dry: dev
        }),
    ]
};

if (true) {
    config.plugins.push(new UglifyJSPlugin({
        sourceMap: true
    }));
    // config.plugins.push(new ManifestPlugin({}))
}

module.exports = config;
