# -*- coding: utf-8 -*-
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

from publishing.models.post import Post
from equipments.models.institution import Institution


@receiver(pre_save, sender=Post)
def auto_generate_post_slug(sender, instance, **kwargs):
    instance.generate_slug()


@receiver(post_save, sender=Institution)
def create_post_handler(sender, instance, created, **kwargs):
    if not created:
        return
    post = Post(title=instance.name, description=instance.description,
                slug=instance.slug, institution=instance, status='PUB', is_active=True)
    post.save()
