# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from publishing.models.post import Post


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):

    save_on_top = True
    list_display = ('title', 'publisher', 'status', 'created_at', 'verif_status', 'is_active',)
    list_filter = ('is_active', 'verif_status', 'status', 'publisher', 'created_at',)
    search_fields = ('publisher', 'title',)
    prepopulated_fields = {'slug': ('title',)}
    readonly_fields = ('created_at', 'modified_at')
