# -*- coding: utf-8 -*-
from django.contrib.syndication.views import Feed
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from publishing.models.post import Post


class LastPost(Feed):

    title = _('Actualités de la plateforme Kojump')
    link = '/rss/'
    description = 'Les dernières institutions ajouté à la plateforme Kojump'

    def items(self):
        return Post.objects.order_by('created_at')[:10]

    def item_title(self, item):
        return item.post.title

    def item_description(self, item):
        return item.post.description

    def item_link(self, item):
        return reverse('news-item', args=[item.pk])

