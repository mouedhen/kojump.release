# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.generic import ListView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin

from publishing.models.post import Post


class OwnerPostListView(LoginRequiredMixin, ListView):

    model = Post
    template_name = 'publishing/posts_list.html'

    def get_queryset(self):
        return Post.objects.get_publisher_post(user=self.request.user)
