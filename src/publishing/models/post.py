# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from slugify import slugify
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.db import models

from publishing.models.postable import Postable
from equipments.models.institution import Institution


class DraftPostsManager(models.Manager):

    def get_queyset(self):
        return super(DraftPostsManager, self).get_queryset().filter(status='DRAFT')


class PublishedPostsManager(models.Manager):

    def get_queyset(self):
        return super(PublishedPostsManager, self).get_queryset().filter(status='PUB')


class PostManager(models.Manager):

    def get_publisher_post(self, user):
        return self.filter(publisher=user).order_by('-created_at')


# TODO add institution to posts
@python_2_unicode_compatible
class Post(Postable):

    objects = PostManager()
    draft = DraftPostsManager()
    published = PublishedPostsManager()

    class Meta:
        verbose_name = _(u'Publication')
        verbose_name_plural = _(u'Publications')

    STATUS = (
        ('DRAFT', _(u'Brouillon')),
        ('PUB', _(u'Publié(e)')),
    )

    title = models.CharField(_(u'titre'), max_length=254,)
    slug = models.SlugField(_(u'slug'), max_length=254,)
    description = models.TextField(_(u'description'), blank=True, null=True)
    status = models.CharField(_(u'statut'), max_length=5, choices=STATUS, default='DRAFT')
    verif_status = models.BooleanField(_(u'est vérifié ?'), max_length=5, default=False)

    institution = models.OneToOneField(Institution, on_delete=models.CASCADE, related_name='post')

    def generate_slug(self):
        self.slug = slugify(self.title)

    def __str__(self):
        return self.title
