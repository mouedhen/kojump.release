# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.core.exceptions import PermissionDenied

from django.contrib.auth.models import User


class Postable(models.Model):

    class Meta:
        abstract = True

    publisher = models.ForeignKey(User, null=True, blank=True)
    created_at = models.DateTimeField(_(u'créé(e) le'), auto_now_add=True)
    modified_at = models.DateTimeField(_(u'modifié(e) le'), auto_now=True)
    is_active = models.BooleanField(_(u'est active ?'), default=True)

    def set_publisher(self, user):
        if not user:
            raise PermissionDenied
        self.created_by = user

    def get_published(self, user):
        return self.objects.get(publisher=user)
