# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.db import models

from publishing.models.post import Post
from publishing.models.postable import Postable


@python_2_unicode_compatible
class Favorite(Postable):

    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='favorites')

    class Meta:
        verbose_name = _(u'Favori')
        verbose_name_plural = _(u'Favoris')

    def __str__(self):
        return self.post
