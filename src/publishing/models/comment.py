# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.db import models

from publishing.models.postable import Postable
from publishing.models.post import Post


@python_2_unicode_compatible
class Comment(Postable):

    STATUS = (
        ('NO', _(u'Non modéré')),
        ('YES', _(u'modéré')),
    )

    class Meta:
        verbose_name = _(u'Commentaire')
        verbose_name_plural = _(u'Commentaires')

    comment = models.TextField(_(u'commentaire'))
    status = models.CharField(_(u'statut'), max_length=3, choices=STATUS, default='NO')
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments')

    def __str__(self):
        return self.comment
