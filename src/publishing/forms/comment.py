# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django import forms
from django.db import transaction

from publishing.models.comment import Comment


class CreateCommentForm(forms.ModelForm):

    comment = forms.CharField(widget=forms.Textarea(attrs={
        'placeholder': _(u'Ecrire un commentaire...'), 'rows': '5'}))

    class Meta:
        model = Comment
        fields = ['comment']
