# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class PublishingConfig(AppConfig):
    name = 'publishing'

    def ready(self):
        from . import signals
