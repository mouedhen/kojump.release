# -*- coding: utf-8 -*-

from django.conf.urls import url
from publishing.views.post import OwnerPostListView

app_name = 'publishing'

urlpatterns = [
    # Included to accounts
    url(r'^accounts/$', OwnerPostListView.as_view(), name='owner_publication'),
]
