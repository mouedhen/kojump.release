# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json

from django.contrib.gis.geos import Point
from django.contrib.gis.measure import Distance
from django.views.generic import View
from django.http import HttpResponse
from django.shortcuts import redirect

from equipments.models.institution import Institution
from equipments.models.activity import Activity


class SearchService(View):

    def get(self, request, *args, **kwargs):

        distance = 10
        lat = 44.90191
        lng = -0.47306
        activities = None
        is_public = False
        is_always_open = False
        only_season = False

        filters = dict(request.GET)

        if 'distance' in filters:
            distance = filters['distance'][0]

        if 'activities' in filters:
            activities = filters['activities']

        if not activities:
            activities = Activity.objects.filter(is_active=True)

        if ('lat' in filters) and ('lng' in filters):
            lng = filters['lng'][0]
            lat = filters['lat'][0]

        if 'is_public' in filters and filters['is_public'][0] == 'on':
            is_public = True

        if 'is_always_open' in filters and filters['is_always_open'][0] == 'on':
            is_always_open = True

        if 'only_season' in filters and filters['only_season'][0] == 'on':
            only_season = True

        point = Point(float(lng), float(lat), srid=4326)
        kwargs = {
            'equipments__gps_coordinates__distance_lte': (point, Distance(km=float(distance))),
            'equipments__is_public': is_public,
            'equipments__is_always_open': is_always_open,
            'equipments__only_season': only_season,
            'equipments__disciplines__activity_id__in': activities
        }

        institutions = Institution.objects.filter(**kwargs).distinct()

        results = []
        palette = ['#556270', '#4ECDC4', '#C7F464', '#FF6B6B', '#C44D58']
        icon = ['star', 'marker']
        import random

        for institution in institutions:
            if institution.coordinates:
                r5 = random.randint(0, 4)
                r2 = random.randint(0, 1)
                institution_json = {
                    'type': 'Feature',
                    'geometry': {
                        'type': 'Point',
                        'coordinates': [institution.coordinates.x, institution.coordinates.y]
                    },
                    'properties': {
                        'id': institution.pk,
                        'thumbnail': institution.thumbnail.url,
                        'name': institution.name,
                        'slug': institution.slug,
                        'address': institution.address,
                        'marker-color': palette[r5],
                        'marker-size': "small",
                        'marker-symbol': icon[r2]
                    }
                }
                results.append(institution_json)
        geojson_data = {
            'type': 'FeatureCollection',
            'crs': {
                'type': 'name',
                'properties': {'name': 'EPSG:4326'}
            },
            'features': results
        }
        data = json.dumps(geojson_data)

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)
