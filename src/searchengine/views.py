# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.handlers.wsgi import WSGIRequest
from django.shortcuts import redirect
from django.views.generic import TemplateView
from equipments.forms.activity import MultiCheckboxActivitiesForm


class SearchView(TemplateView):

    template_name = 'search/search.html'

    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data(**kwargs)
        context['form_activities'] = MultiCheckboxActivitiesForm
        return context

    def get(self, request, *args, **kwargs):
        if request.GET.get('previous') == '/':
            filters = dict(request.GET)
            if 'activities' in filters:
                activities = filters['activities']
                if len(activities) == 1:
                    return redirect('activity-detail', activities[0])
        return super(SearchView, self).get(request, *args, **kwargs)
